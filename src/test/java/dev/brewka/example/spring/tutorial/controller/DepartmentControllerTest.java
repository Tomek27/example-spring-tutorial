package dev.brewka.example.spring.tutorial.controller;

import dev.brewka.example.spring.tutorial.database.entity.Department;
import dev.brewka.example.spring.tutorial.service.DepartmentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;

@WebMvcTest(DepartmentController.class)
class DepartmentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DepartmentService departmentService;

    private Department department;

    @BeforeEach
    void setUp() {
        department = Department.builder()
                .address("Ahmedabad@localhost")
                .code("IT-06")
                .name("IT")
                .build();
        department.setId(1);
    }

    @Test
    void save() throws Exception {
        Department inputDep = Department.builder()
                .address("Ahmedabad@localhost")
                .code("IT-06")
                .name("IT")
                .build();

        Mockito.when(departmentService.save(inputDep)).thenReturn(department);

        String inputJson = """
                { "name": "IT", "code": "IT-06", "address": "Ahmedabad@localhost" }""";
        mockMvc.perform(MockMvcRequestBuilders.post("/departments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(inputJson))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void byId() throws Exception {
        Mockito.when(departmentService.byId(1)).thenReturn(department);

        mockMvc.perform(MockMvcRequestBuilders.get("/departments/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name")
                        .value(department.getName()));
    }
}