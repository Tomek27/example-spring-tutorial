package dev.brewka.example.spring.tutorial.database.repo;

import dev.brewka.example.spring.tutorial.database.entity.Department;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class DepartmentRepoTest {

    @Autowired
    private DepartmentRepo departmentRepo;

    @Autowired
    private TestEntityManager entityManager;

    @BeforeEach
    void setUp() {
        Department dep1 = Department.builder()
                .name("IT").code("IT-7").address("Berlin").build();
        dep1.setId(1);
        entityManager.persist(dep1);
    }

    @Test
    public void whenFindByIdThenReturnDepartment() {
        Department department = departmentRepo.findById(1).get();
        assertEquals("IT", department.getName());
    }
}