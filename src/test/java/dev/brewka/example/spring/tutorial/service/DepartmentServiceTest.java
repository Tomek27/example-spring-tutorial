package dev.brewka.example.spring.tutorial.service;

import dev.brewka.example.spring.tutorial.database.entity.Department;
import dev.brewka.example.spring.tutorial.database.repo.DepartmentRepo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class DepartmentServiceTest {

    @Autowired
    private DepartmentService departmentService;

    @MockBean
    private DepartmentRepo departmentRepo;

    @BeforeEach
    void setUp() {
        Department dep1 = Department.builder()
                .name("IT").address("Ahmedad").code("IT").build();
        Mockito.when(departmentRepo.findByName("IT")).thenReturn(Optional.of(dep1));
    }

    @Test
    @DisplayName("Get Data base on validate Department name")
    public void whenValidDepartmentNameThenDepartmentShouldFound() {
        String name = "IT";
        Department found = departmentService.byName(name);
        assertEquals(name, found.getName());
    }
}