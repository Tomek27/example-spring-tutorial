package dev.brewka.example.spring.tutorial.service;

import dev.brewka.example.spring.tutorial.database.entity.Department;
import dev.brewka.example.spring.tutorial.database.repo.DepartmentRepo;
import dev.brewka.example.spring.tutorial.except.NotFoundException;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class DepartmentServiceImpl implements DepartmentService {

    private final DepartmentRepo departmentRepo;

    @Override
    public Department save(Department department) {
        return departmentRepo.saveAndFlush(department);
    }

    @Override
    public List<Department> all() {
        return departmentRepo.findAll();
    }

    @Override
    public Department byId(Integer id) throws NotFoundException {
        return departmentRepo.findById(id).orElseThrow(() -> new NotFoundException("Department not found"));
    }

    @Override
    public void deleteById(Integer id) {
        departmentRepo.deleteById(id);
    }

    @Override
    public Department update(Integer id, Department department) {
        Department departmentDb = departmentRepo.findById(id).orElse(null);
        if (departmentDb != null) {
            if (Strings.isNotBlank(department.getName())
                    && !department.getName().equalsIgnoreCase(departmentDb.getName())) {
                departmentDb.setName(department.getName());
            }
            if (Strings.isNotBlank(department.getAddress())
                && !department.getAddress().equalsIgnoreCase(departmentDb.getAddress())) {
                departmentDb.setAddress(department.getAddress());
            }
            if (Strings.isNotBlank(department.getCode())
                && !department.getCode().equalsIgnoreCase(departmentDb.getCode())) {
                departmentDb.setCode(department.getCode());
            }
            return departmentRepo.saveAndFlush(departmentDb);
        } else {
            // Entity not found
            return null;
        }
    }

    @Override
    public Department byName(String name) {
        return departmentRepo.findByName(name).orElse(null);
    }
}
