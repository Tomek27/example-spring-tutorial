package dev.brewka.example.spring.tutorial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExampleSpringTutorialApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExampleSpringTutorialApplication.class, args);
	}

}
