package dev.brewka.example.spring.tutorial.except;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ExceptionMessage {

    private HttpStatus status;
    private String message;
}
