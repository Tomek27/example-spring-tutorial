package dev.brewka.example.spring.tutorial.service;

import dev.brewka.example.spring.tutorial.database.entity.Department;
import dev.brewka.example.spring.tutorial.except.NotFoundException;

import java.util.List;

public interface DepartmentService {
    Department save(Department department);
    List<Department> all();
    Department byId(Integer id) throws NotFoundException;

    void deleteById(Integer id);

    Department update(Integer id, Department department);

    Department byName(String name);
}
