package dev.brewka.example.spring.tutorial.controller;

import dev.brewka.example.spring.tutorial.database.entity.Department;
import dev.brewka.example.spring.tutorial.except.NotFoundException;
import dev.brewka.example.spring.tutorial.service.DepartmentService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Log4j2
@RestController
@RequestMapping("/departments")
@AllArgsConstructor
public class DepartmentController {

    private final DepartmentService departmentService;

    @PostMapping
    public Department save(@Valid @RequestBody Department department) {
        log.info("Saving entity: {}", department);
        return departmentService.save(department);
    }

    @GetMapping
    public List<Department> getAll() {
        return (List<Department>) departmentService.all();
    }

    @GetMapping("/{id}")
    public Department byId(@PathVariable Integer id) throws NotFoundException {
        log.info("Entity by ID: {}", id);
        return departmentService.byId(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Integer id) {
        log.info("Delete entity by ID: {}", id);
        departmentService.deleteById(id);
    }

    @PutMapping("/{id}")
    public Department update(@PathVariable Integer id, @RequestBody Department department) {
        log.info("Update entity by ID: {} with: {}", id, department);
        return departmentService.update(id, department);
    }

    @GetMapping("/name/{name}")
    public Department byName(@PathVariable String name) {
        log.info("Entity by name: {}", name);
        return departmentService.byName(name);
    }
}
