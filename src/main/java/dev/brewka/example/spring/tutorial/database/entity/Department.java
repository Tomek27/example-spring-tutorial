package dev.brewka.example.spring.tutorial.database.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.*;

@Entity
@Table
@Getter @Setter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Department extends AbstractEntity {

    @NotBlank(message = "name cannot be blank")
    @Column
    private String name;
    @Email
    @NotBlank(message = "address cannot be blank")
    @Column
    private String address;
    @NotBlank(message = "code cannot be blank")
    @Column
    private String code;
}
